const jwt = require("jsonwebtoken");
const secret ="CourseBookingAPI";

//JSON Web Token or JWT is a way of securely passing info from the server to the front end, or to other parts of server

//Info is kept secure through the use of secret code
//only the system that knows the secret code that can decode the encrypted information

//Token Creation
/*
-Analogy
	Pack the gift and provide a lock with the secret code as the key
*/
//'module.exports' - para mareuse sa ibang file ung function na ginagawa natin dito...
module.exports.createAccessToken = (user) => {
	//the data will be received from the registration form
	//when the user logs in, a token will be created with the user's info
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	//Generate a JSON web token using the jwt's method (sign())
	return jwt.sign(data, secret, {})
	/*-eto ung signature part, si header ay default na
	{} - dito nakalagay ung options, like expiration ng token*/
}


//Token Verification

/*
-Analogy
	Receive the gift and open the lock to verify if the sender is legitimate and the gift was not tampered

*/
//since verification siya, may marereceive tayong request then magsesend siya ng response
module.exports.verify = (req,res,next) => {

	//The token is retrieved from the request header
	//sa Postman, katabi nung body, may Auth saka headers
	let token = req.headers.authorization; /* <-as is eto*/

	//Token received and is not undefined
	if(typeof token !== "undefined") {

		console.log(token);
		//Bearer xxxxsdfoashfoisahfaisfhasof. - laging may prefix na 'Bearer ' string. Para maalis siya, gamit ng slice method and maobtain lang ung token.
		token = token.slice(7,token.length);

		//Validate the token using the "verify" method decrypting the token using the secret code

		return jwt.verify(token, secret, (err,data) => {
			//if JWT is  not valid
			if(err){
				return res.send({auth: "failed"})
			}else {
				//if JWT is valid
				next() /*kinakoll lang ung middleware fxn then nagproproceed na siya sa next statement. Pag nabasa to, pupunta na siya sa next below statement punta na siya sa module.exports.decode*/
				}
		})

	} else {
		//if token does not exist
		return res.send({auth : "token undefined"})
	}

}

//Token Decryption
/*
-Analogy
Open the gift and get the content. So makukuha na natin ung information
*/

module.exports.decode = (token) => {

	//Token Received and is not undefined
	if(typeof token !== "undefined"){
		//Retrieves only the token and removes the "Bearer " prefix
		token = token.slice(7,token.length);

		return jwt.verify(token,secret, (err, data) => {
			if (err) {
				return null;
			} else {
				return jwt.decode(token, {complete: true}).payload
			}
		})
	}else{
		//token does not exist
		return null;
	}

}