const Course = require("../models/Course");

//Create a new course
/*
Steps: (pwede rin na magvalidation muna bago magcreate)
1. Create a new Course object
2. Save to the database
3. Error handling

*/

module.exports.addCourse = (reqBody) =>{

console.log(reqBody);
                                 
	//Create a new object
	let newCourse = new Course ({
		name: reqBody.course.name,
		description: reqBody.course.description,
		price: reqBody.course.price
	});

	//Saves the created object
	return newCourse.save().then((course,error) => {
		//Course Creation failed
		if(error) {
			return false;
		}else{
			//if course creation is successful
			return true;
		}
	})
} 
//Sir Orlando answer
// module.exports.addCourse = (reqBody) => {

// 	console.log(reqBody);

// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	})

// 	return newCourse.save().then((course, error) => {
// 		if (error) {
// 			return false;
// 		} else{
// 			return true;
// 		}
// 	})
// }



//S35 - retrieving all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then( result => {
		return result;
	})
}


//Retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then( result => {
		return result;
	})
}

//Retrieve a specific course
//may pinapasa tayong reqParams from Route
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then( result => {
		return result;
	})
}
//Update a course
/*
Steps: 
1. Create a variable which will contain the information retrieved from the request body (kasi hindi lang isang property ung iuupdate natin, depende sa kung ano ung ibibigay ng client)
2. find and update course using the course ID
*/

module.exports.updateCourse = (courseId,reqBody) => {
	//Specify muna ung mga properties to be updated
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	//Find by Id and Update(id, updatesToBeApplied)
	return Course.findByIdAndUpdate(courseId,updatedCourse).then((course,error) => {
		//couse not updated
		if(error){
			return false;
		}else{
			//course updated successfully
			return true;
		}
	})
}

module.exports.archiveCourse = (courseId,reqBody) => {
	//Specify muna ung mga properties to be updated
	let archiveCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive:false
	};

	//Find by Id and Update(id, updatesToBeApplied)
	return Course.findByIdAndUpdate(courseId,archiveCourse).then((course,error) => {
		//couse not updated
		if(error){
			return false;
		}else{
			//course updated successfully
			return true;
		}
	})
}